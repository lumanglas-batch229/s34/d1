// Default Setup of Express
// "Require to load the module/package"

const express = require("express");

// Create of an App/Project
// In layman's terms, app is our server
const app = express();
const port = 3000;

// Setup for allowing the server to handle data from request
// Middlewares
// 

app.use(express.json()); //allowing system to handle API

// Allows your app to read data from forms

app.use(express.urlencoded({extended:true}));

// [SECTION] - Routes

// GET Method

app.get("/",(req, res) => {
	res.send("Hello World!");
});

// POST Method
app.post("/hello",(req, res) => {
	res.send(`Hello There ${req.body.firstname} ${req.body.lastName}!`);
});

// MOCK Database 
// An array that will store user object when the "/signup" route is accessed 
// This will server as our mock database

let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	//If the username and password is not empty the user info will be pushed stored to our mock database 
	if (req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	}else {
		res.send("Please input both username and password")
	}
});

// PUT /UPDATE METHOD

app.put("/change-password", (req, res) => {
	// Creates a variable to store the message to be sent back to the client.
	let message;
	// Create a for loop that will loop through the elements of "users" array
	for (let i = 0; i < users.length; i++){
		// if the username provided in the client/postman and the username of the current object is the same.
		if (req.body.username == users[i].username){
			// Changes the password of the user found by the loop
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`;
			break;
		} else {
			// If no user found
		}	message = "User does not exist.";
	}
	
	res.send(message);
});

// CODE ALONG ACTIVITY

// HOME page
app.get("/home", (req, res) => {
	res.send("Welcome to Homepage")
});

// Registered Users
app.get("/users", (req, res) => {
	res.send(users)
});

// DELETE USER
app.delete("/delete-user", (req, res) => {
	let message;
	if (users.length != 0) {
		for (let i = 0; i < users.length; i++){
			if (req.body.username == users[i].username){
				users.splice(users[i], 1);
				message = `User ${req.body.username} has been deleted`;
				break;
			}else {
				message = "User does not exist.";
			}
		}
	}else {
		message = "Users is Empty";
	}
	res.send(message);
})

app.listen(port, () => console.log(`Server is running at port ${port}.`));

